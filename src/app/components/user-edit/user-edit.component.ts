import { Component, OnInit } from '@angular/core';
import { user } from 'src/app/models/user';
import { UserService } from '../../services/user.service';
import { global } from '../../services/global'

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
  providers: [UserService]
})
export class UserEditComponent implements OnInit {

  public page_title: string
  public user: user
  public identity
  public token
  public status
  public url
  public resetVar
  public froala_options: Object = {
    placeholderText: 'Cuéntanos sobre ti',
    charCounterCount: true,
    language: 'es',
    toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat'],
    toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat'],
    toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat'],
    toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat']
  }
  public afuConfig = {
      uploadAPI: {
        url: global.url+'user/upload',
        headers: {
          "Authorization": this._userService.getToken()
        }
      },
      maxSize: "50",
      formatsAllowed: ".jpg, .png, .jpeg, .gif",
      theme: "attachPin",
      hideProgressBar: false,
      multiple: false,
      hideResetBtn: true,
      hideSelectBtn: false,
      fileNameIndex: true,
      replaceTexts: {
        selectFileBtn: 'Selecciona tu imagen',
        resetBtn: 'Reiniciar',
        uploadBtn: 'Subir imagen',
        dragNDropBox: 'Arratra aquí tu imagen',
        attachPinBtn: 'Sube tu avatar de usuario',
        afterUploadMsg_success: 'Subido con éxito!',
        afterUploadMsg_error: 'Error al subir!',
        sizeLimit: 'Size Limit'
      }
  };

  constructor(
    private _userService: UserService //uso del servicio
  ) { 
    this.page_title = 'Ajustes de usuario'
    this.user = new user(1, '', '', 'ROLE_USER', '', '', '', '');
    this.identity = this._userService.getIdentity()
    this.token = this._userService.getToken()
    this.url = global.url
    // rellenar objeto usuario
    this.user = new user(
        this.identity.sub,
        this.identity.name,
        this.identity.surname,
        this.identity.role,
        this.identity.email,
        '',
        this.identity.description,
        this.identity.image,
    )
  }

  ngOnInit() {
    
  }

  onSubmit(form){
    this._userService.updated(this.token, this.user).subscribe(
      response => {
        if(response && response.status){
          console.log(response)
          this.status = 'success'
          // actualizar usuario en sesion
          if(response.changes.name){
            this.user.name = response.changes.name
          }
          if(response.changes.surname){
            this.user.surname = response.changes.surname
          }
          if(response.changes.email){
            this.user.email = response.changes.email
          }
          if(response.changes.description){
            this.user.description = response.changes.description
          }
          if(response.changes.image){
            this.user.image = response.changes.image
          }
          this.identity = this.user
          localStorage.setItem('identity', JSON.stringify(this.identity))
        } else {

        }
      },
      error => {
        this.status = 'error'
        console.log(<any>error)
      }
    )
  }

  avatarUpload(datos){
    let data = JSON.parse(datos.response)
    this.user.image = data.image
  }

}
