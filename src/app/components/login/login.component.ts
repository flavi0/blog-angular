import { Component, OnInit } from '@angular/core';
import { user } from '../../models/user';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {
  public page_title: string;
  public user: user;
  public status: string;
  public token;
  public identity
  constructor(
    private _userService: UserService,
    private _router: Router,
    private _route: ActivatedRoute
  ) { 
    this.page_title = 'Identifícate'
    this.user = new user(1, '', '', 'ROLE_USER', '', '', '', '');
  }

  ngOnInit() {
    // se ejecuta siempre y cierra sesion solo cuando le llega el parametro 'sure' por la url
    this.logout()
  }

  onSubmit(form){
    this._userService.signup(this.user).subscribe(
      response => {
        // devuelve token
        if(response.status != 'error'){
          this.status = 'success'
          this.token = response

          // devuelve los datos del usuario:
          this._userService.signup(this.user, true).subscribe(
            res => {
              this.identity = res
              console.log(this.token)
              console.log(this.identity)

              // persistir datos del usuario identificado
              localStorage.setItem('token', this.token)
              localStorage.setItem('identity', JSON.stringify(this.identity)) // localstorage guarda texto no objeto
              // redireccion a inicio:
              this._router.navigate(['inicio'])
            },
            error => {
              this.status = 'error'
              console.error(<any>error)
            }
          )

        } else {
          this.status = 'error'
        }
      },
      error => {
        this.status = 'error'
        console.error(<any>error)
      }
    )
  }

  logout(){
    this._route.params.subscribe(params => {
      let logout = +params['sure'] // obtener el parametro de la ruta, se obtiene un string, se agregar el '+' para casting-Int
      if(logout == 1) {
        // borrar los datos de localstorage, parecido a cerrar sesión
        localStorage.removeItem('identity')
        localStorage.removeItem('token')

        // vaciar datos de la clase:
        this.identity = null,
        this.token = null

        // redireccion a inicio:
        this._router.navigate(['inicio'])
      }
    })
  }

}
