import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { category } from '../../models/category'
import { CategoryService } from '../../services/category.service'
import { UserService } from '../../services/user.service'
import { PostService } from '../../services/post.service'
import { global } from '../../services/global'
import { post } from 'src/app/models/post';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css'],
  providers: [CategoryService, UserService, PostService]
})
export class CategoryDetailComponent implements OnInit {

  public url
  public page_title: string
  public category: category
  public posts: Array<post>
  public identity
  public token

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _categoryService: CategoryService,
    private _postService: PostService,
    private _userService: UserService
  ) { 
    this.url = global.url
    this.identity = this._userService.getIdentity()
    this.token = this._userService.getToken()
  }

  ngOnInit() {
    this.getPostsByCategory()
  }

  getPostsByCategory(){
    // recoger parametro de la url
    this._route.params.subscribe(params =>{
      let id = +params['id']
      
      // peticion al servicio:
      this._categoryService.getCategory(id).subscribe(
        response => {
          console.log(response)

          if(response.status == 'success'){
            console.log('getCategory----correcto')
            this.category = response.category
            //this.posts = response.posts
            this._categoryService.getPostsBycategory(id).subscribe(
              response => {
                if(response.status == 'success'){
                  this.posts = response.posts
                  console.log(this.posts)
                } else {
                  console.log('error en xddd')
                  this._router.navigate(['/inicio'])
                }
              },
              error => {
                console.error(error);
              }
            )

          } else {
            this._router.navigate(['/inicio'])
          }
        },
        error => {
          console.error(error);
        }
      )
    })
  }
  deletePost(id){
    this._postService.delete(this.token, id).subscribe(
      response => {
        this.getPostsByCategory() // volver a cargar el listado de posts
      },
      error => {
        console.error(error);
      }
    )
  }

}
