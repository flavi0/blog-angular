import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { UserService } from '../../services/user.service'
import { CategoryService } from '../../services/category.service'
import { PostService } from '../../services/post.service'
import { post } from '../../models/post'
import { global } from '../../services/global'

@Component({
  selector: 'app-post-edit',
  templateUrl: '../post-new/post-new.component.html',
  providers: [UserService, CategoryService, PostService]
})
export class PostEditComponent implements OnInit {

  public page_title: string
  public identity
  public token
  public post: post
  public categories
  public status
  public resetVar
  public is_edit:boolean
  public url: string

  public froala_options: Object = {
    placeholderText: 'Escribe aquí sobre tu artículo informativo.',
    charCounterCount: true,
    indentWithTabs: true,
    lineNumbers: true,
    lineWrapping: true,
    mode: 'text/html',
    tabMode: 'indent',
    tabSize: 2,
    toolbarButtons: ['bold', 'italic', 'underline'],
    toolbarButtonsXS: ['bold', 'italic', 'underline'],
    toolbarButtonsSM: ['bold', 'italic', 'underline'],
    toolbarButtonsMD: ['bold', 'italic', 'underline']
  }

  public afuConfig = {
    uploadAPI: {
      url: global.url+'post/upload',
      headers: {
        "Authorization": this._userService.getToken()
      }
    },
    maxSize: "50",
    formatsAllowed: ".jpg, .png, .jpeg, .gif",
    theme: "attachPin",
    hideProgressBar: false,
    multiple: false,
    hideResetBtn: true,
    hideSelectBtn: false,
    fileNameIndex: true,
    replaceTexts: {
      selectFileBtn: 'Selecciona tu imagen',
      resetBtn: 'Reiniciar',
      uploadBtn: 'Subir imagen',
      dragNDropBox: 'Arratra aquí tu imagen',
      attachPinBtn: 'Agrega alguna imagen a tu post',
      afterUploadMsg_success: 'Subido con éxito!',
      afterUploadMsg_error: 'Error al subir!',
      sizeLimit: 'Size Limit'
    }
};

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _categoryService: CategoryService,
    private _postService: PostService
  ) {
    this.page_title = 'Editar entrada'
    this.identity = this._userService.getIdentity()
    this.token = this._userService.getToken()
    this.is_edit = true
    this.url = global.url
  }

  ngOnInit() {
    this.getCategories()
    this.post = new post(1, this.identity.sub, 1, '', '', null, null, null, null)
    this.getPost()
  }
  onSubmit(form) {
    this._postService.updated(this.token, this.post, this.post.id).subscribe(
      response => {
        if(response.status == 'success') {
          this.status = 'success'
          //redireccionar a la pagina del post
          this._router.navigate(['/entrada/'+this.post.id])
        }
      },
      error => {
        console.log('update -- error!!!!')
        console.log(error)
      }
    )
  }

  getCategories(){
    this._categoryService.getCategories().subscribe(
      response => {
        if(response.status == 'success'){
          this.categories = response.categories
        }
      },
      error => {
        console.log(error)
      }
    )
  }
  imageUpload(datos){
    let image_data = JSON.parse(datos.response)
    this.post.image = image_data.image
  }
  getPost(){
    // sacar el id del post desde la url
    this._route.params.subscribe(params => {
      let id = +params['id']
      // peticion ajax para sacar los datos
      this._postService.getPost(id).subscribe(
        response => {
          if(response.status == 'success'){
            if(response.post.user_id != this.identity.sub){
              this._router.navigate(['/inicio'])
            }
            this.post = response.post
          } else {
            this._router.navigate(['/inicio'])
          }
        },
        error => {
          console.log(error)
          this._router.navigate(['/inicio'])
        }
      )
    })
  }

}
