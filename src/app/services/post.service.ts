import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { post } from '../models/post';
import { global } from './global'

// para que se use como servicio, inyectando configuracion de dependencia
@Injectable()
export class PostService {
    public url: string
    constructor(
        private _http: HttpClient
    ){
        this.url = global.url
    }

    prueba(){
        return 'desde servicio de entrada'
    }

    created(token, post): Observable<any>{
        // limpiar campo content: htmlEntities --> utf-8
        post.content = global.htmlEntities(post.content)
        post.content = post.content.split('&nbsp;').join(' ')
        delete post.category
        delete post.user
        let json = JSON.stringify(post)
        let params = "json="+json
        let headers = new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .set('Authorization', token)
        console.log('***************************************')
        console.log(post.content)
        console.log('***************************************')
        return this._http.post(this.url+'post', params, {headers})
    }

    getPosts(): Observable<any>{
        let headers = new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')

        return this._http.get(this.url+'post',{headers})
    }

    getPost(id): Observable<any>{
        let headers = new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')

        return this._http.get(this.url+'post/'+id,{headers})
    }

    updated(token, post, id):Observable<any> {
        post.content = global.htmlEntities(post.content)
        /*delete post.category
        delete post.user*/
        let json = JSON.stringify(post)
        let params = "json="+json
        let headers = new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', token)
        console.log('***************************************')
        console.log(post)
        console.log('***************************************')
        return this._http.put(this.url+'post/'+id, params, {headers})
    }

    delete(token, id){
        let headers = new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', token)
        return this._http.delete(this.url+'post/'+id, {headers})
    }

}
