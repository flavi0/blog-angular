import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { post } from '../../models/post'
import { category } from '../../models/category'
import { PostService } from '../../services/post.service'
import { UserService } from '../../services/user.service'
import { global } from '../../services/global'

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css'],
  providers: [PostService, UserService]
})
export class PostDetailComponent implements OnInit {

  public post:post
  public url
  public identity

  constructor(
    private _postService: PostService,
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.url = global.url
    this.identity = this._userService.getIdentity()
  }

  ngOnInit() {
    this.getPost()
  }

  getPost(){
    // sacar el id del post desde la url
    this._route.params.subscribe(params => {
      let id = +params['id']
      console.log(id)
      // peticion ajax para sacar los datos
      this._postService.getPost(id).subscribe(
        response => {
          if(response.status == 'success'){
            this.post = response.post
            console.log(this.post)
          } else {
            this._router.navigate(['/inicio'])
          }
        },
        error => {
          console.log(error)
          this._router.navigate(['/inicio'])
        }
      )
    })
  }

}
