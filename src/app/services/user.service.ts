import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { user } from '../models/user';
import { global } from './global'

// para que se use como servicio, inyectando configuracion de dependencia
@Injectable()
export class UserService {
    public url: string
    public identity
    public token
    constructor(
        public _http: HttpClient
    ){
        this.url = global.url
    }

    test(){
        return 'Hola mundo desde servicio xD'
    }

    register(user): Observable<any>{
        console.log('service-register')
        let json = JSON.stringify(user)
        let params = 'json='+json

        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')

        // hacer petición ajax
        return this._http.post(this.url+'register', params, {headers})
    }

    signup(user, getToken=null): Observable<any>{
        console.log('service-login')
        if(getToken != null) {
            user.getToken = 'true'
        }
        let json = JSON.stringify(user)
        let params = 'json='+json
        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        return this._http.post(this.url+'login', params, {headers})
    }

    updated(token, user): Observable<any> {
        // limpiar campo content: htmlEntities --> utf-8
        user.description = global.htmlEntities(user.description)
        let json = JSON.stringify(user)
        let params = "json="+json // formato para envio de datos por post
        let headers = new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', token)

        // peticion:
        return this._http.put(this.url+'user/update', params, {headers})
    }

    getIdentity () {
        let identity = JSON.parse(localStorage.getItem('identity'))
        if(identity && identity != undefined) {
            this.identity = identity
        } else {
            this.identity = null
        }
        return this.identity
    }

    getToken() {
        let token = localStorage.getItem('token')
        if(token && token != undefined){
            this.token = token
        } else {
            this.token = null
        }
        return this.token
    }

    getPostsByUser(id):Observable<any>{
        let headers = new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')
        return this._http.get(this.url+'post/user/'+ id, {headers})
    }

    getUser(id):Observable<any>{
        let headers = new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')
        return this._http.get(this.url+'user/detail/'+ id, {headers})
    }
}

