import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { UserService } from '../../services/user.service'
import { CategoryService } from '../../services/category.service'
import { PostService } from '../../services/post.service'
import { post } from '../../models/post'
import { global } from '../../services/global'

@Component({
  selector: 'app-post-new',
  templateUrl: './post-new.component.html',
  styleUrls: ['./post-new.component.css'],
  providers: [UserService, CategoryService, PostService]
})
export class PostNewComponent implements OnInit {

  public page_title: string
  public identity
  public token
  public post: post
  public categories
  public status
  public resetVar
  public is_edit:boolean
  public url: string

  public froala_options: Object = {
    language: 'es',
    placeholderText: 'Escribe aquí sobre tu artículo informativo.',
    charCounterCount: true,
    indentWithTabs: true,
    lineNumbers: true,
    lineWrapping: true,
    mode: 'text/html',
    tabMode: 'indent',
    tabSize: 2,
    toolbarButtons: ['bold', 'italic', 'underline'],
    toolbarButtonsXS: ['bold', 'italic', 'underline'],
    toolbarButtonsSM: ['bold', 'italic', 'underline'],
    toolbarButtonsMD: ['bold', 'italic', 'underline']
  }

  public afuConfig = {
    uploadAPI: {
      url: global.url+'post/upload',
      headers: {
        "Authorization": this._userService.getToken()
      }
    },
    maxSize: "50",
    formatsAllowed: ".jpg, .png, .jpeg, .gif",
    theme: "attachPin",
    hideProgressBar: false,
    multiple: false,
    hideResetBtn: true,
    hideSelectBtn: false,
    fileNameIndex: true,
    replaceTexts: {
      selectFileBtn: 'Selecciona tu imagen',
      resetBtn: 'Reiniciar',
      uploadBtn: 'Subir imagen',
      dragNDropBox: 'Arratra aquí tu imagen',
      attachPinBtn: 'Agrega alguna imagen a tu post',
      afterUploadMsg_success: 'Subido con éxito!',
      afterUploadMsg_error: 'Error al subir!',
      sizeLimit: 'Size Limit'
    }
};

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _categoryService: CategoryService,
    private _postService: PostService
  ) {
    this.page_title = 'Crear una entrada'
    this.identity = this._userService.getIdentity()
    this.token = this._userService.getToken()
  }

  ngOnInit() {
    this.getCategories()
    this.post = new post(1, this.identity.sub, 1, '', '', null, null, null, null)
  }
  onSubmit(form) {
    console.log(this.post)
    console.log(this._postService.prueba())
    this._postService.created(this.token, this.post).subscribe(
      response => {
        if(response.status == 'success'){
          this.post = response.post
          this.status = 'success'
          this._router.navigate(['/inicio'])
        } else {
          this.status = 'success'
        }
      },
      error => {
        console.error(error);
        this.status = 'error'
      }
    )
  }

  getCategories(){
    this._categoryService.getCategories().subscribe(
      response => {
        if(response.status == 'success'){
          this.categories = response.categories
        }
      },
      error => {
        console.log(error)
      }
    )
  }
  imageUpload(datos){
    let image_data = JSON.parse(datos.response)
    this.post.image = image_data.image
  }

}
