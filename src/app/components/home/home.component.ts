import { Component, OnInit } from '@angular/core';
import { post } from '../../models/post'
import { PostService } from '../../services/post.service'
import { UserService } from '../../services/user.service'
import { global } from '../../services/global'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [PostService, UserService]
})
export class HomeComponent implements OnInit {
  public page_title: string
  public url
  public posts: Array<post>
  public identity
  public token

  constructor(
    private _postService: PostService,
    private _usertService: UserService
  ) { 
    this.page_title = 'Inicio';
    this.url = global.url
    this.identity = this._usertService.getIdentity()
    this.token = this._usertService.getToken()
  }

  ngOnInit() {
    this.getPosts()
  }

  getPosts(){
    this._postService.getPosts().subscribe(
      response=>{
        if(response.status = 'success'){
          this.posts = response.posts
          console.log(this.posts)
          console.log('*******' + response.status + '-----')
        }
      },
      error => {
        console.error(error);
        
      }
    )
  }
  deletePost(id){
    this._postService.delete(this.token, id).subscribe(
      response => {
        this.getPosts() // volver a cargar el listado de posts
      },
      error => {
        console.error(error);
      }
    )
  }

}
