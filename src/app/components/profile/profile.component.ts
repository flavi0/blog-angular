import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { post } from '../../models/post'
import { user } from '../../models/user'
import { PostService } from '../../services/post.service'
import { UserService } from '../../services/user.service'
import { global } from '../../services/global'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [PostService, UserService]
})
export class ProfileComponent implements OnInit {
  public url
  public posts: Array<post>
  public user: user
  public identity
  public token
  public empty_post: boolean

  constructor(
    private _postService: PostService,
    private _usertService: UserService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.url = global.url
    this.identity = this._usertService.getIdentity()
    this.token = this._usertService.getToken()
  }

  ngOnInit() {
    this.getProfile()
  }

  getProfile() {
    // sacar el id del post desde la url
    this._route.params.subscribe(params => {
      let userId = +params['id']
      this.getPosts(userId)
      this.getUser(userId)
    })
  }

  getUser(userId) {
    this._usertService.getUser(userId).subscribe(
      response => {
        if(response.status == 'success') {
          this.user = response.user
          console.log(this.user)
        }
      },
      err => {
        console.log(err)
      }
    )
  }

  getPosts(userId){
    this._usertService.getPostsByUser(userId).subscribe(
      response=>{
        if(response.status = 'success'){
          if(response.posts.length == 0) {
            this.empty_post = true
          } else {
            this.posts = response.posts
          }
          console.log(response)
        }
      },
      error => {
        console.error(error);
        
      }
    )
  }
  deletePost(id){
    this._postService.delete(this.token, id).subscribe(
      response => {
        this._route.params.subscribe(params => {
          let userId = +params['id']
          this.getProfile()
        })
      },
      error => {
        console.error(error);
      }
    )
  }

}
